
package ThreadDeadlock;

/**
 *
 * @author Ahsan
 */
public class ThreadDeadlock {
    static class Friend {
        private final String name;
        public Friend(String name) {
            this.name = name;
        }
        public String getName() {
            return this.name;
        }
        public synchronized void bow(Friend bower) {
            System.out.println(Thread.currentThread().getName());
            System.out.format("%s: %s"
                + "  has bowed to me!%n", 
                this.name, bower.getName());
            // In first thread when this(Alphonse) and bower(Gaston)
            // In second thread when this(Gaston) and bower(Alphonse)            
            // In first thread this call trying to access bower(Gaston) 
            // [First Thread in Monitor/Waiting State]
            // In second thread this call trying to access bower(Alphonse) 
            // [Second Thread in Monitor/Waiting State]
            
            bower.bowBack(this);
        }
        public synchronized void bowBack(Friend bower) {
            System.out.println("Called bowBack()");
            System.out.format("%s: %s"
                + " has bowed back to me!%n",
                this.name, bower.getName());
        }
    }

    public static void main(String[] args) {
        
        final Friend alphonse = new Friend("Alphonse");
        final Friend gaston = new Friend("Gaston");
        // Thread-0
        new Thread(new Runnable() {
            @Override
            public void run() { alphonse.bow(gaston); }
        }).start();
        // Thread-1
        new Thread(new Runnable() {
            @Override
            public void run() { gaston.bow(alphonse); }
        }).start();
        System.out.println("Main thread exits");
    }
}